<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return View('index', ['view' => 'home', 'title' => 'Beranda']);
    }

    public function aboutUs()
    {
        return View('aboutus', ['view' => 'aboutus', 'title' => 'Tentang kami']);
    }

    public function product()
    {
        return View('product', ['view' => 'product', 'title' => 'Produk']);
    }

    public function contactus()
    {
        return View('contactus', ['view' => 'contactus', 'title' => 'Kontak kami']);
    }
}
