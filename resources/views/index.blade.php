@extends('layout')
@section('content')

<div class="col-md-8">
  <div class="row">
  <h2>Beranda Ni yee</h2>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </div>
  <div class="row" style="margin-top: 6%">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Produk Baru</a></li>
      <li><a data-toggle="tab" href="#menu1">Banyak diminati</a></li>
      <li><a data-toggle="tab" href="#menu2">Banyak dibeli</a></li>
    </ul>
    <div class="tab-content">
      <div id="home" class="tab-pane fade in active">
        <h3>Produk Baru</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
      <div id="menu1" class="tab-pane fade">
        <h3>Banyak diminati</h3>
        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      </div>
      <div id="menu2" class="tab-pane fade">
        <h3>Banyak dibeli</h3>
        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
      </div>
    </div>

  </div>
</div>

<div class="col-md-4">
  <div class="row" style="margin-top: 6%; margin-left: 6%">
    <div class="input-group">
      <span class="input-group-addon" id="basic-addon1">
        <span class="glyphicon glyphicon-search"></span>
      </span>
      <input type="text"
             class="form-control"
             placeholder="cari produk"
             aria-describedby="basic-addon1"
             id="search">

    </div>
  </div>

  <div class="row" style="margin-top: 6%; margin-left: 6%">
    <div class="list-group">
      <a href="#" value="bapak budi" class="list-group-item">bapak budi</a>
      <a href="#" value="ibu budi" class="list-group-item">ibu budi</a>
      <a href="#" value="nenek budi" class="list-group-item">nenek budi</a>
      <a href="#" value="kakek budi" class="list-group-item">kakek budi</a>
      <a href="#" value="anak budi" class="list-group-item">anak budi</a>
    </div>
  </div>

</div>

  @endsection
