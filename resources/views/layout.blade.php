<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PEPE INC | {{$title}}</title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}">

  </head>
  <body>
    <div class="navbar-wrapper">
        <div class="container">

          <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a href="{{ url('') }}">
                  <img class="navbar-brand" src="{{ URL::asset('img/pepe_logo.png') }}" alt="" />
                </a>
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                  <li class="{{ $view == 'home' ? 'active' : '' }}"><a href="{{ url('') }}">Beranda</a></li>
                  <li class="{{ $view == 'aboutus' ? 'active' : '' }}"><a href="{{ url('aboutus') }}">Tentang Kami</a></li>
                  <li class="{{ $view == 'product' ? 'active' : '' }}"><a href="{{ url('product') }}">Produk</a></li>
                  <li class="{{ $view == 'contactus' ? 'active' : '' }}"><a href="{{ url('contactus') }}">Kontak Kami</a></li>
                </ul>
              </div>
            </div>
          </nav>
          <hr>

          @yield('content')
          <div id="footer">


            <b>&copy; 2016</b>

            <a href="{{ url('') }}"> <u>Beranda</u> </a>
            | <a href="{{ url('aboutus') }}"> <u>Tentang Kami</u> </a>
            | <a href="{{ url('product') }}"> <u>Produk</u> </a>
            | <a href="{{ url('contactus') }}"> <u>Kontak Kami</u> </a>


          </div>

        </div>




      </div>


      <script src="{{ URL::asset('js/jquery-3.1.1.min.js') }}"></script>
      <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
      <script src="{{ URL::asset('js/main.js') }}"></script>
  </body>
</html>
